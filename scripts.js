class DataService {
    constructor() {
        this.baseURL = 'http://81.2.241.234:8080';
        this.Hero = new HeroDataService(this.baseURL);
        this.Scpecies = new SpeciesDataService(this.baseURL);
    }
}

class HeroDataService {
    constructor(baseURL) {
        this.baseURL = baseURL;
    }

    GetAll() {
        return $.get(this.baseURL + '/hero', results => {
            return results;
        });
    }
}

class SpeciesDataService {
    constructor(baseURL) {
        this.baseURL = baseURL;
    }

    GetAll() {
        return $.get(this.baseURL + '/species', results => {
            return results;
        });
    }
}
